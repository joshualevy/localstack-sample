# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/archive" {
  version     = "2.0.0"
  constraints = "~> 2.0.0"
  hashes = [
    "h1:eOUi4EO4QTgPuz+gmD8xy2LTTxNfyc9txyc9PDARth8=",
    "zh:1c3300a6686481ed0b3ce456949805b5c55f901b77108543046f12118a102914",
    "zh:2d44bcc8a5ea3f2c2ff54ca4c4b273cdc3500ad8bb6929eec6722bb9d10a9714",
    "zh:4be17c4e06a74ca30a3c9e88446e453ad16c493b2ddef872a6580dbfe618a1b4",
    "zh:50c216a6c672b51d67ece37ce69c121acbcd5c8d24b899e876c61bc09989f69f",
    "zh:90d6ad51fecab8d2f086aab9ed45020f1a9d3627dbc95edda11af8e3f87082d9",
    "zh:d147427135330b4940a85a0f552e2c1f83a0d1ccdcc82c36a2d311b7f85dbd38",
    "zh:dd4b8c0b113e37fd83ceab0f8a203ccfa39caaca5551c70a94fa61e900902932",
    "zh:e30ef64a2ed0c2a6e8d1e29b8da8b05b4a303e256478a4b410af81c81cbcbc23",
    "zh:e5a81379810d3e1f380b7145d0ecee4a69ab4e80184f77b66cd4411b86aca6a8",
    "zh:f9a0a2a72721e7e047e83053173b3a68a7d53e2a22719cc2d9234dbee46c466c",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "4.13.0"
  constraints = ">= 3.4.0, ~> 4.13.0"
  hashes = [
    "h1:DK3+k7Yxeenw945TqdHD2RBHcgbxpM1Z+Cw5Q4mndKw=",
    "zh:215226bc0372077d2ae6dba4e2f08f6361f8e4953d20bc4c682d40fdf5002544",
    "zh:42777cbdc046181986c0260ea17027ef1364c31d73a57eb0ab539f6e1a3e0780",
    "zh:78079d2f5fc35f3c43eb2a131cb49c2c77ddd04943bca97080f33355808d39cc",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9c0404a044eae741f10f3d217dc28658e0f04082963918913b024d3305c11e79",
    "zh:a1b5a53f60d4f7bff1cc84180fef6205c95b8793741dbc8c0564a6200424ca73",
    "zh:ba6711064a855ddb55924342b70667e9bed660bde8552dc0bde4b7f8947a2ec4",
    "zh:d0f77ed514d54f7380d7e1ef585d853f50f1bee381d6abbf3a68429b68de6045",
    "zh:d5c454d2ac9aed01ae00c477192c93d54c8362357a87684a3171055dcec25f44",
    "zh:dfd381ed7da945cb85b99df843ee7eab339dd1799fa70d1ad3e94331605aad01",
    "zh:eb6dc84414714f61b9de0ac190c69f598af9b16d144a44f573df484c06c8d4ef",
    "zh:f02e79599af3f8f63e4b885c5715be3a4060cbf98eb4bf46d616aa0d9f2b5cd3",
  ]
}

provider "registry.terraform.io/kreuzwerker/docker" {
  version     = "3.0.1"
  constraints = "~> 3.0.1"
  hashes = [
    "h1:zdKNQX3dhcV+sPOG4kwK9ll6nthKPPIUTIMF3Xav3d8=",
    "zh:02f60126ca16b344092df3c315296bf1a216c3b2a68eddb3c89fdfa5ea826118",
    "zh:0d2ee9624a54dbc10538b0c4e296348641b9bfba1354b3f872e43f7ec69a75f2",
    "zh:473d7427da8c9efc231266abc7fdc27fca5f9ee0bdfcdb9914f0a2886e3e23b8",
    "zh:5f0189bcd0c944c001098cb17a23efa79df8f0eec8644a64fe0e4200983ba5b7",
    "zh:6200319c41d6baad3f46701a4028412f8ae2496e29fc4fef9584cc71da5fbbe6",
    "zh:650be621f2216b1240f148eae8fcf80ec57c35925e2b212db7c23a70b9e67e06",
    "zh:72fcfa6207251105066a34f0ec6d27ecc658b565e84fa946da376dd1afadd265",
    "zh:92fc352a2090d3d380c7c8e8bbdf6f99d93a0182701056bb1d2dbfd5049e8ca6",
    "zh:a7e2ef666c2a7eb5661b06cfbd7635cb9543524e7bf6a3851dcf6eacc9950cc4",
    "zh:a8604595e61e8919c51a8656800c8c64557f9a2bc00309315895b380f2e9be19",
    "zh:caf65603a84b749d8f3af2ee47b66f7e21d481f981e2e1d1d59838751c5e3be4",
    "zh:dad40c4e57da284e7f57b5c0cc9dfac3cb27b01d2f2436fbe3464f0a2111b262",
    "zh:dc1b173dbcba9d74879b16f36f6d9e97ef62fbd6fca8db79ec4fe4ec69c0e2f3",
    "zh:e506d04677383b6d62bd69d42dc9005e27a45ccc2efc6e0de607e1f8445981d2",
  ]
}
