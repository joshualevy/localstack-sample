variable "product_abbreviation" {
  type        = string
  description = "Value of the product_abbreviation variable"
}

variable "env_name" {
  type        = string
  description = "Value of the env_name variable"
}

variable "env_region" {
  type        = string
  description = "The region in which to create/manage resources"
  default     = "us-east-1"
}

variable "partition" {
  description = "Govcloud (aws-us-gov) or Commercial (aws) partition used to pass in for api-gateway template file"
  type        = string
}

variable "custom_tags" {
  description = "Custom tags for tagging resources"
  type        = map(any)
  default     = {}
}
