resource "aws_cloudwatch_metric_alarm" "lambda_alarms" {
  alarm_name          = var.environment == "prd" ? var.alarm_name : format("%s: %s", var.environment, var.alarm_name)
  alarm_description   = var.alarm_description
  comparison_operator = var.comparison_operator
  evaluation_periods  = var.evaluation_periods
  threshold           = var.threshold
  period              = var.period

  namespace   = var.namespace
  metric_name = var.metric_name
  statistic   = var.statistic

  dimensions = var.dimensions

  alarm_actions = var.alarm_actions
}
