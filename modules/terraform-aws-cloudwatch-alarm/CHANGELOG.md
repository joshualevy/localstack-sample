# v1.0.0

- Initial commit

## v1.1.0

- Adding stage arn to the outputs.tf

## v1.2.0

- Updated the default value for cache control header variable

## v1.3.0

- Minimized the number for outputs and variables
