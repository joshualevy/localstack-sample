terraform {
  # Optional attributes and the defaults function are
  # both experimental, so we must opt in to the experiment.
  experiments = [module_variable_optional_attrs]
}

variable "alarm_name" {
  description = "Name of the cloudwatch alarm"
  type        = string
}

variable "alarm_description" {
  description = "Description to the cloudwatch alarm"
  type        = string
}

variable "comparison_operator" {
  description = "comparison operator to compare the threshold value"
  type        = string
}

variable "evaluation_periods" {
  description = "No of periods to evaluate the threshold limit"
  type        = string
}

variable "threshold" {
  description = "threshold limit of the cloudwatch alarm"
  type        = string
}

variable "period" {
  description = "Interval limit for the metric"
  type        = string
}

variable "namespace" {
  description = "Name of the service to create cloudwatch alarm"
  type        = string
}

variable "metric_name" {
  description = "Metric name for the particular namespace"
  type        = string
}

variable "statistic" {
  description = "statistic value to see the graph, e.g: sum, average or maximum etc"
  type        = string
}

variable "alarm_actions" {
  description = "list of alarm actions to trigger the SNS"
  type        = list(string)
  default     = []
}

variable "dimensions" {
  description = "map of dimensions to create lambda functions coudwatch alarm"
  type = object({
    FunctionName = optional(string),
    ApiName      = optional(string),
    Stage        = optional(string),
    TableName    = optional(string)
  })
  # default = [{
  #   hash_key : "REC_timestamp",
  #   range_key : "REC_state"
  # }]
}

variable "environment" {
  description = "Name of environment where alarm is deployed"
  type        = string
}


/* variable "lambda_dimensions" {
  description = "map of dimensions to create lambda functions coudwatch alarm"
  type = object({
    FunctionName  = string
  })
  # default = [{
  #   hash_key : "REC_timestamp",
  #   range_key : "REC_state"
  # }]
}

variable "api_gateway_dimensions" {
  description = "map of dimensions to create api gateway coudwatch alarm"
  type = object({
    ApiName  = string,
    Stage    = string
  })
  # default = [{
  #   hash_key : "REC_timestamp",
  #   range_key : "REC_state"
  # }]
} */
