output "dynamodb_info" {
  description = "The name, ARN and invoke ARN of the DynamoDB Table"
  value = {
    dynamodb_name       = aws_dynamodb_table.dynamodb_table.name
    dynamodb_arn        = aws_dynamodb_table.dynamodb_table.arn
    dynamodb_stream_arn = aws_dynamodb_table.dynamodb_table.stream_arn
  }
}
