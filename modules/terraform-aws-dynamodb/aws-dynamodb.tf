resource "aws_dynamodb_table" "dynamodb_table" {
  name             = var.table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = var.hash_key
  range_key        = var.range_key
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  dynamic "attribute" {
    for_each = var.attributes
    content {
      name = attribute.value
      type = "S"
    }
  }

  dynamic "attribute" {
    for_each = var.typed_attributes
    content {
      name = attribute.value.name
      type = attribute.value.type
    }
  }

  # ttl {
  #   attribute_name = "TimeToExist"
  #   enabled        = false
  # }

  dynamic "local_secondary_index" {
    for_each = var.local_secondary_index
    content {
      name            = "${local_secondary_index.value}-index"
      projection_type = "ALL"
      range_key       = local_secondary_index.value
    }
  }

  dynamic "global_secondary_index" {
    for_each = var.global_secondary_index
    content {
      name            = global_secondary_index.value.name != "" ? global_secondary_index.value.name : "${global_secondary_index.value.hash_key}-index"
      hash_key        = global_secondary_index.value.hash_key
      range_key       = global_secondary_index.value.range_key
      projection_type = "ALL"
      read_capacity   = 0
      write_capacity  = 0
    }
  }

  # replica {
  #   region_name = var.dr_region
  # }

  tags = {
    Name        = "GasOperations"
    Environment = var.env_name
  }
}

/* resource "aws_dynamodb_table_replica" "dynamodb_table_replica" {
  provider         = "aws.west"
  global_table_arn = aws_dynamodb_table.dynamodb_table.arn

  tags = {
    Name        = "GasOperations"
    Environment = var.env_name
  }
} */


