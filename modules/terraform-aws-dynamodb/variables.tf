variable "table_name" {
  description = "Name of the dynamodb table"
  type        = string
}

variable "hash_key" {
  description = "Primary key of the dynamodb table"
  type        = string
}

variable "range_key" {
  description = "Sort key of the dynamodb table"
  type        = string
}

variable "env_name" {
  description = "Environment name for the tag"
  type        = string
}

variable "attributes" {
  description = "list of attributes"
  type        = list(string)
  default     = []
}

variable "typed_attributes" {
  description = "attributes with typed specified"
  type = map(object({
    name = string
    type = string
  }))
  default = {}
}

variable "local_secondary_index" {
  description = "list of Local secondary indexes"
  type        = list(string)
  default     = []
}

variable "global_secondary_index" {
  description = "map of Gobal secondary indexes"
  type = list(object({
    name      = string,
    hash_key  = string,
    range_key = string
  }))
  default = [{
    name : "REC_timestamp-index",
    hash_key : "REC_timestamp",
    range_key : "REC_state"
  }]
}

variable "dr_region" {
  type        = string
  description = "The region for Disaster Recovery operations"
  default     = "us-west-2"
}
