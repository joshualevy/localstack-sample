
data "archive_file" "lambda_zip_file_int" {
  type        = "zip"
  output_path = var.output_path
  source_dir  = var.source_dir

  #source_dir  = "templates/s3-proxy/./"
  #output_path = "templates/s3-proxy.zip"

}

resource "aws_lambda_function" "function" {
  description                    = var.function_info.function_description
  function_name                  = var.function_info.function_name
  role                           = aws_iam_role.lambda_exec.arn
  runtime                        = var.function_info.function_runtime
  reserved_concurrent_executions = var.reserved_concurrent_executions
  handler                        = var.function_info.function_handler
  publish                        = var.publish
  timeout                        = var.timeout
  memory_size                    = var.memory
  layers                         = var.layers_arn

  vpc_config {
    subnet_ids         = var.vpc_subnet_ids
    security_group_ids = var.vpc_security_group_ids
  }

  filename         = data.archive_file.lambda_zip_file_int.output_path
  source_code_hash = data.archive_file.lambda_zip_file_int.output_base64sha256

  dynamic "environment" {
    for_each = length(keys(var.environment_variables)) == 0 ? [] : [
    true]
    content {
      variables = var.environment_variables
    }
  }

  # dynamic "vpc_config" {
  #   for_each = length(var.vpc_subnet_ids) > 0 ? [true] : []
  #   # && length(var.vpc_security_group_ids) > 0
  #   content {
  #     subnet_ids         = var.vpc_subnet_ids
  #     security_group_ids = ["sg-00f0d81b954670afa"]
  #   }
  # }

  tags = merge({
    ManagedByTFE = "True"
  }, var.custom_tags)

  lifecycle {
    ignore_changes = [
      source_code_hash,
      filename,
    ]
  }
}
