output "lambda_info" {
  description = "The name, ARN and invoke ARN of the Lambda Function"
  value = {
    lambda_name       = aws_lambda_function.function.function_name
    lambda_arn        = aws_lambda_function.function.arn
    lambda_invoke_arn = aws_lambda_function.function.invoke_arn
    lambda_role_id    = aws_iam_role.lambda_exec.id
    lambda_role_arn   = aws_iam_role.lambda_exec.arn
  }
}
