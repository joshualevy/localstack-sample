variable "custom_tags" {
  description = "Custom tags for tagging resources"
  type        = map(any)
  default     = {}
}

variable "iam_policy_name" {
  description = "unique iam policy name for each lambda call"
  type        = string
}

variable "function_info" {
  description = "Object containing Lambda function name and description, e.g. {function_name = <Lambda Function Name>, function_runtime = <Runtime Value, for details check out the available runtimes at https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime> , function_description = <Lambda Function Description>, function_handler = <Lambda Function entrypoint in your code>}"
  type = object({
    function_name        = string
    function_runtime     = string
    function_description = string
    function_handler     = string
  })
}

variable "publish" {
  description = "Whether to publish creation/change as new Lambda Function Version."
  type        = bool
  default     = true
}

variable "reserved_concurrent_executions" {
  description = "The amount of reserved concurrent executions for this Lambda Function. A value of 0 disables Lambda Function from being triggered and -1 removes any concurrency limitations. Defaults to Unreserved Concurrency Limits -1."
  type        = number
  default     = -1
}

variable "timeout" {
  description = "The amount of time your Lambda Function has to run in seconds."
  type        = number
}

variable "environment_variables" {
  description = "A map that defines environment variables for the Lambda Function."
  type        = map(string)
  default     = {}
}

variable "memory" {
  description = "Amount of memory(in MB) the Lambda Function can use at runtime. A value from 128 MB to 3,008 MB, in 64 MB increments."
  type        = number
  default     = 128
}

variable "iam_logs_resources" {
  description = "arn for commercial (arn:aws:logs:*:*:*) or govcloud (arn:aws-us-gov:logs:*:*:*) for logs"
  type        = string
  default     = "arn:aws:logs:*:*:*"
}

variable "iam_policies" {
  description = "iam permissions for lambdas e.g. organizations:createGovCloudAccount"
  type        = list(string)
  default     = []
}

# variable "file_path_zip" {
#   description = "zipped file path for each lambda"
#   type        = string
# }

# variable "file" {
#   description = "file name for each lambda"
#   type        = string
# }

# variable "file_path" {
#   description = "file path for each lambda"
#   type        = string
# }

variable "source_dir" {
  description = "Package entire contents of this directory into the archive, unique iam policy name for each lambda call"
  type        = string
}

variable "output_path" {
  description = "The output of the archive file, unique iam policy name for each lambda call"
  type        = string
}

variable "vpc_subnet_ids" {
  description = "List of subnet ids where the lambda should run in the VPC. Usually private or intra subnets."
  type        = list(string)
  default     = []
}

variable "vpc_security_group_ids" {
  description = "List of security group ids for the lambda running in the VPC."
  type        = list(string)
  default     = []
}
variable "layers_arn" {
  description = "Optional list of layer arns to be used with lambda function"
  type        = list(string)
  default     = []
}
