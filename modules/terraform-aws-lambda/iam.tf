resource "aws_iam_role" "lambda_exec" {
  name               = "${var.function_info.function_name}-exec-${data.aws_region.current.name}"
  assume_role_policy = data.aws_iam_policy_document.lambda_exec.json

  tags = merge({
    ManagedByTFE = "True"
  }, var.custom_tags)
}

data "aws_iam_policy_document" "lambda_exec" {
  version = "2012-10-17"

  statement {
    sid = "LambdaExecutionRole"

    actions = ["sts:AssumeRole"]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com", "states.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "base_policy" {
  name        = "${var.iam_policy_name}-policy-${data.aws_region.current.name}"
  description = "IAM policy to allow lambda to create govcloud"

  policy = data.aws_iam_policy_document.base_policy.json
}

data "aws_iam_policy_document" "base_policy" {
  version = "2012-10-17"

  dynamic "statement" {
    for_each = length(var.iam_policies) == 0 ? [] : [true]
    content {
      sid = "LambdaAccountPermissions"

      actions = var.iam_policies

      effect = "Allow"

      resources = ["*"]
    }
  }

  statement {
    sid = "AllowCreationOfServiceLinkedRoles"

    actions = [
      "iam:CreateServiceLinkedRole"
    ]
    effect = "Allow"

    resources = ["*"]
  }

  statement {
    sid = "AllowLambdasAssumeRole"

    actions = [
      "sts:AssumeRole"
    ]
    effect = "Allow"

    resources = ["*"]
  }

  statement {
    sid = "LambdaLogGroupPermissions"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    effect = "Allow"

    resources = [
      "${var.iam_logs_resources}"
    ]
  }

  statement {
    sid = "VpcEc2"
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "ec2:AssignPrivateIpAddresses",
      "ec2:UnassignPrivateIpAddresses"
    ]
    effect    = "Allow"
    resources = ["*"]
  }

  statement {
    sid = "AccesSecretManagerPolicy"
    actions = [
      "secretsmanager:GetSecretValue"
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_role_policy_attachment" "base" {
  role       = aws_iam_role.lambda_exec.name
  policy_arn = aws_iam_policy.base_policy.arn
}
