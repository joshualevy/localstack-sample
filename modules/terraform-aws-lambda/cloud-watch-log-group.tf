resource "aws_cloudwatch_log_group" "lambda_logs" {
  name              = "/aws/lambda/${var.function_info.function_name}"
  retention_in_days = 30

  tags = merge({
    ManagedByTFE = "True"
  }, var.custom_tags)
}