# AWS Lambda Function

AWS Lambda is an event-driven, serverless computing platform provided by Amazon as a part of Amazon Web Services. It is a computing service that runs code in response to events and automatically manages the computing resources required by that code.

This component creates:

- A Lambda Function
- A Execution IAM Role for lambda function with base policies
  - policies include organizational permissions, CW logs permissions, and KMS permissions
- Zipped file of lambda python function.

## Architecture

![Architecture Diagram](lambda-function-reusable.png)

## Run-Book

### Pre-requisites

#### IMPORTANT NOTE

1. Required version of Terraform and Providers are mentioned in `versions.tf`.
2. Go through `variables.tf` for understanding each terraform variable before running this component.

#### AWS Accounts

Needs the following accounts:

1. Account (AWS account where lambda function is to be created)

### Getting Started

#### How to use this component in a blueprint

IMPORTANT: We periodically release versions for the components. Since, master branch may have on-going changes, best practice would be to use a released version in form of a tag (e.g. ?ref=x.y.z)

```terraform
module "lambda_name" {
  source                 = "git@gitlab.com:xcel-master/cloud/DOF/components/terraform-aws-lambda.git?ref=v1.0.0"
  function_info          = {
    function_name = '<lambda function name>'
    function_description = '<Lambda Function description>'
    function_runtime = '<lambda function runtime> more details: https://docs.aws.amazon.com/lambda/latest/dg/API_CreateFunction.html#SSS-CreateFunction-request-Runtime'
    function_handler = '<lambda function handler name>'
  }
  timeout                = "10" #in seconds, defaults to 3 sec max 15 mins

  environment_variables  = {
    variable1 = value1
    variable2 = value2
  }
  iam_policy_name = '<iam policy name to be created for lambda function>'
  iam_logs_resources = '<arn for CW logs arn:aws:logs for commercial and arn:aws-us-gov:logs for govcloud>'
  organizations_policies = '<iam policies for org apis>'
  file = '<file name for lambda function>'
  file_path = '<path to file for lambda function>'
  file_path_zip = '<path to zipped file for lambda function>'
  custom_tags            = var.custom_tags
}
```
