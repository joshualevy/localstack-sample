resource "aws_iam_user" "iam_user" {
  name = var.user_name
  path = var.user_path

  tags = var.tags
}

resource "aws_iam_user_policy" "inline_policy" {
  name = "${var.user_name}-inline-policy"
  user = aws_iam_user.iam_user.name

  policy = jsonencode({
    Version   = "2012-10-17"
    Statement = var.policy_statements
  })
}

resource "aws_iam_access_key" "access_key" {
  user = aws_iam_user.iam_user.name
}
