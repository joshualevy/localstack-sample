variable "user_name" {
  type        = string
  description = "Name of the IAM User"
}

variable "user_path" {
  type        = string
  default     = "/"
  description = "Path in which to create the user"
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "policy_statements" {
  type        = list(any)
  default     = []
  description = "A list of policy statements to attach to this user"
}
