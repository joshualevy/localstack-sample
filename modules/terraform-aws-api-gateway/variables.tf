variable "access_log_settings" {
  type        = map(string)
  description = "Enables access logs for the API stage. Detailed below."
  default     = {}
}

variable "_api_id" {
  type    = string
  default = ""
}

variable "api_name" {
  type        = string
  description = "The name of the API"
}

variable "api_key_source" {
  type        = string
  description = "The source of the API key for requests. Valid values are HEADER (default) and AUTHORIZER."
  default     = "HEADER"
}

variable "binary_media_types" {
  type        = list(string)
  description = "The list of binary media types supported by the Api. By default, the Api supports only UTF-8-encoded text payloads."
  default     = []
}

variable "body" {
  type        = string
  description = "An OpenAPI specification that defines the set of routes and integrations to create as part of the API."
  default     = <<EOF
{
  "openapi": "3.0.1",
  "info": {
    "title": "Default API",
    "description": "A skeleton API",
    "version": "1.0"
  }
}
EOF

}

variable "client_certificate_id" {
  type        = string
  description = "The identifier of a client certificate for the stage."
  default     = ""
}

variable "cache_cluster_enabled" {
  type        = string
  description = "Specifies whether a cache cluster is enabled for the stage"
  default     = "false"
}

variable "cache_cluster_size" {
  type        = string
  description = "The size of the cache cluster for the stage, if enabled. Allowed values include 0.5, 1.6, 6.1, 13.5, 28.4, 58.2, 118 and 237."
  default     = "0.5"
}

variable "cache_data_encrypted" {
  type        = bool
  description = "Specifies whether the cached responses are encrypted."
  default     = "false"
}

variable "cache_ttl_in_seconds" {
  type        = number
  description = "Specifies the time to live (TTL), in seconds, for cached responses. The higher the TTL, the longer the response will be cached."
  default     = null
}

variable "caching_enabled" {
  type        = bool
  description = "pecifies whether responses should be cached and returned for requests. A cache cluster must be enabled on the stage for responses to be cached."
  default     = "false"
}

variable "custom_tags" {
  description = "Custom tags for tagging resources"
  type        = map(any)
  default     = {}
}

variable "data_trace_enabled" {
  type        = bool
  description = "Specifies whether data trace logging is enabled for this method, which effects the log entries pushed to Amazon CloudWatch Logs."
  default     = "false"
}

variable "deploy_description" {
  type        = string
  description = "The description of the deployment"
  default     = ""
}

variable "deploy_variables" {
  type        = map(string)
  description = "A map that defines variables for the stage"
  default     = {}
}

variable "description" {
  type        = string
  description = "The description of the API"
  default     = ""
}

variable "destination_arn" {
  type        = string
  description = "ARN of the log group to send the logs to. Automatically removes trailing :* if present."
  default     = ""
}

variable "documentation_version" {
  type        = string
  description = "The version of the associated API documentation"
  default     = ""
}

variable "format" {
  type        = string
  description = "The formatting and values recorded in the logs. For more information on configuring the log format rules"
  default     = ""
}

variable "logging_level" {
  type        = string
  description = "Specifies the logging level for this method, which effects the log entries pushed to Amazon CloudWatch Logs. The available levels are OFF, ERROR, and INFO."
  default     = "ERROR"
}

variable "method_path" {
  type        = string
  description = "Method path defined as {resource_path}/{http_method} for an individual method override, or */* for overriding all methods in the stage."
  default     = "*/*"
}

variable "metrics_enabled" {
  type        = bool
  description = " Specifies whether Amazon CloudWatch metrics are enabled for this method. (treu or false)"
  default     = "false"
}

variable "minimum_compression_size" {
  type        = string
  description = "Minimum response size to compress for the API. Integer between -1 and 10485760 (10MB). Setting a value greater than -1 will enable compression, -1 disables compression (default)."
  default     = ""
}

variable "partition" {
  description = "Govcloud (aws-us-gov) or Commercial (aws) partition used to pass in for api-gateway template file"
  type        = string
}

variable "policy" {
  type        = string
  description = "JSON formatted policy document that controls access to the API Gateway. For more information about building AWS IAM policy documents with Terraform"
  default     = ""
}

variable "require_authorization_for_cache_control" {
  type        = bool
  description = "Specifies whether authorization is required for a cache invalidation request."
  default     = "false"
}

variable "settings" {
  type        = map(string)
  description = "The settings block, see below."
  default     = {}
}

variable "stage_description" {
  type        = string
  description = "The description of the stage"
  default     = ""
}

variable "stage_name" {
  type        = string
  description = "The name of the stage"
}

variable "stage_variables" {
  type        = map(string)
  description = "A map that defines the stage variables"
  default     = {}
}

variable "throttling_burst_limit" {
  type        = number
  description = "Specifies the throttling burst limit."
  default     = null
}

variable "throttling_rate_limit" {
  type        = number
  description = "Specifies the throttling rate limit."
  default     = null
}

variable "types" {
  type        = list(string)
  description = "A list of endpoint types. This resource currently only supports managing a single value. Valid values: EDGE, REGIONAL or PRIVATE. If unspecified, defaults to EDGE. Must be declared as REGIONAL in non-Commercial partitions. Refer to the documentation for more information on the difference between edge-optimized and regional APIs"
  default     = ["PRIVATE"]
}

variable "unauthorized_cache_control_header_strategy" {
  type        = string
  description = "Specifies how to handle unauthorized requests for cache invalidation. The available values are FAIL_WITH_403, SUCCEED_WITH_RESPONSE_HEADER, SUCCEED_WITHOUT_RESPONSE_HEADER."
  default     = "FAIL_WITH_403"
}

variable "usage_plan_burst_limit" {
  type        = number
  description = "Specifies the API request burst limit, the maximum rate limit over a time ranging from one to a few seconds, depending upon whether the underlying token bucket is at its full capacity."
  default     = 2000
}

variable "usage_plan_key_type" {
  type        = string
  description = "Specifies the type of a usage plan key. Currently, the valid key type is API_KEY"
  default     = "API_KEY"
}

variable "usage_plan_rate_limit" {
  type        = number
  description = "Specifies the API request steady-state rate limit."
  default     = 500
}

# variable "vpc_id" {
#   description = "VPC ID where the api gateway vpc endpoint needs to be deployed"
#   type        = string
# }

variable "vpc_subnet_ids" {
  description = "List of subnet ids where the api gateway vpc endpoint should run in the VPC. Usually private or intra subnets."
  type        = list(string)
  default     = null
}

variable "xray_tracing_enabled" {
  type        = string
  description = "Whether active tracing with X-ray is enabled (true or false) . Defaults to false."
  default     = "false"
}

variable "certificate_arn" {
  type        = string
  description = "Certificate ARN from custom ACM module"
}

variable "domain_name" {
  type        = string
  description = "Registered domain name for API endpoint"
}
