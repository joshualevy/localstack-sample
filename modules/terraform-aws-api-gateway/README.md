# AWS API Gateway

Amazon API Gateway is a fully managed service that makes it easy for developers to create, publish, maintain, monitor, and secure APIs at any scale. APIs act as the "front door" for applications to access data, business logic, or functionality from your backend services. Using API Gateway, you can create RESTful APIs and WebSocket APIs that enable real-time two-way communication applications. API Gateway supports containerized and serverless workloads, as well as web applications.

It creates:

- API Gateway
- An API Gateway stage where in the resources would be deployed
- API Gateway stage settings for configuring the stages
- An API Gateway deployment resource to deploy the stages
- API Key addition to the gateway using usage plans

## Architecture

[TODO] Insert Architecture Diagram

## Run-Book

### Pre-requisites

#### IMPORTANT NOTE

1. Required version of Terraform is mentioned in `meta.tf`.
1. Go through `variables.tf` for understanding each terraform variable before running this component.

#### AWS Accounts

Needs the following accounts:

1. Spoke Account (AWS account where API Gateway is to be created)

### Getting Started

#### How to use this component in a blueprint

We periodically release versions for the components. Since, master branch may have on-going changes, best practice would be to use a released version in form of a tag (e.g. ?ref=x.y.z)

```terraform
module "api_gateway" {
  source = "../terraform-aws-api-gateway/"

  api_name    = "${var.estate_name}-rest-api"
  description = "This is the API Gateway for ${var.estate_name}-rest-api"
  body        = file("${path.module}/config/data-platform-v1-oas30-apigateway.yaml")

  stage_name         = var.stage_name
  stage_description  = "This is a stage for ${var.estate_name}-rest-api"
  deploy_description = "This is a stage deployment for ${var.estate_name}-rest-api"

  method_path = var.method_path

  usage_plan_burst_limit = var.usage_plan_burst_limit
  usage_plan_rate_limit  = var.usage_plan_rate_limit
  usage_plan_key_type    = var.usage_plan_key_type

  charge_code = var.charge_code
  custom_tags = var.custom_tags

  provider = {
    aws = aws.spoke
  }
}
```