# Terraform version lock, account-level data sources, module-globals, and other metadata

terraform {
  required_version = ">= 0.12.6"
}

data "aws_caller_identity" "current" {
  provider = aws
}

data "aws_region" "current" {
  provider = aws
}

data "aws_partition" "current" {
  provider = aws
}

locals {
  tags = merge({
    Name = var.api_name
  }, var.custom_tags)
}
