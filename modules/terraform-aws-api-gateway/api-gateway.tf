resource "aws_api_gateway_rest_api" "gateway" {
  name           = var.api_name
  api_key_source = var.api_key_source
  description    = var.description
  body           = var.body

  policy = <<EOF
  {
      "Version": "2012-10-17",
      "Statement": [
          {
              "Effect": "Allow",
              "Principal": "*",
              "Action": "sts:AssumeRole",
              "Resource": [
                  "*"
              ]
          }
      ]
  }
  EOF

  tags = local.tags
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_deployment" "deployment" {
  description       = var.deploy_description
  stage_description = var.stage_description
  rest_api_id       = aws_api_gateway_rest_api.gateway.id
  variables         = var.deploy_variables

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.gateway.body))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "stage" {
  stage_name            = var.stage_name
  description           = var.stage_description
  rest_api_id           = aws_api_gateway_rest_api.gateway.id
  deployment_id         = aws_api_gateway_deployment.deployment.id
  variables             = var.stage_variables
  cache_cluster_size    = var.cache_cluster_size
  cache_cluster_enabled = var.cache_cluster_enabled
  client_certificate_id = var.client_certificate_id
  xray_tracing_enabled  = var.xray_tracing_enabled
  tags                  = local.tags

  # lifecycle {
  #   ignore_changes = [
  #     deployment_id,
  #   ]
  # }
}


resource "aws_api_gateway_method_settings" "settings" {
  rest_api_id = aws_api_gateway_rest_api.gateway.id
  stage_name  = aws_api_gateway_stage.stage.stage_name
  method_path = "*/*"

  settings {
    logging_level                              = var.logging_level
    data_trace_enabled                         = var.data_trace_enabled
    metrics_enabled                            = var.metrics_enabled
    throttling_burst_limit                     = var.throttling_burst_limit
    throttling_rate_limit                      = var.throttling_rate_limit
    caching_enabled                            = var.caching_enabled
    cache_ttl_in_seconds                       = var.cache_ttl_in_seconds
    cache_data_encrypted                       = var.cache_data_encrypted
    require_authorization_for_cache_control    = var.require_authorization_for_cache_control
    unauthorized_cache_control_header_strategy = var.unauthorized_cache_control_header_strategy
  }
}

resource "aws_api_gateway_usage_plan" "usage_plan" {
  name        = "${var.api_name}-usage-plan"
  description = "This is an API Gateway Usage Plan for ${var.api_name}"

  api_stages {
    api_id = aws_api_gateway_rest_api.gateway.id
    stage  = aws_api_gateway_stage.stage.stage_name
  }

  throttle_settings {
    burst_limit = var.usage_plan_burst_limit
    rate_limit  = var.usage_plan_rate_limit
  }
}

resource "aws_api_gateway_api_key" "key" {
  name = "${var.api_name}-key"
}

resource "aws_api_gateway_usage_plan_key" "usage_plan_key" {
  key_id        = aws_api_gateway_api_key.key.id
  key_type      = var.usage_plan_key_type
  usage_plan_id = aws_api_gateway_usage_plan.usage_plan.id
}

resource "aws_api_gateway_domain_name" "current" {
  regional_certificate_arn = var.certificate_arn
  domain_name              = var.domain_name
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_base_path_mapping" "custom_domain_mapping" {
  api_id      = aws_api_gateway_rest_api.gateway.id
  domain_name = aws_api_gateway_domain_name.current.id
  stage_name  = aws_api_gateway_stage.stage.stage_name
  base_path   = aws_api_gateway_stage.stage.stage_name
}

resource "aws_api_gateway_account" "api_gateway" {
  cloudwatch_role_arn = aws_iam_role.api_gateway_cloudwatch.arn
}

resource "aws_iam_role" "api_gateway_cloudwatch" {
  name = "api_gateway_cloudwatch_global"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "api_gateway" {
  name = "api_gateway_cloudwatch"
  role = aws_iam_role.api_gateway_cloudwatch.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents",
                "logs:GetLogEvents",
                "logs:FilterLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
