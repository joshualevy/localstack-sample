output "arn" {
  description = "The arn of the apigateway rest api"
  value       = aws_api_gateway_rest_api.gateway.arn
}

output "id" {
  description = "The arn of the apigateway rest api"
  value       = aws_api_gateway_rest_api.gateway.id
}

output "name" {
  description = "The name of the apigateway rest api"
  value       = aws_api_gateway_rest_api.gateway.name
}

output "invoke_url" {
  description = "The invoke url of the apigateway rest api"
  value       = aws_api_gateway_deployment.deployment.invoke_url
}

output "execution_arn" {
  description = "The execution arn of the apigateway rest api"
  value       = aws_api_gateway_deployment.deployment.execution_arn
}

output "stage_id" {
  description = "The id of the stage"
  value       = aws_api_gateway_stage.stage.id
}

output "stage_name" {
  description = "The name of the stage"
  value       = aws_api_gateway_stage.stage.stage_name
}

output "stage_arn" {
  description = "The arn of the stage"
  value       = aws_api_gateway_stage.stage.arn
}

# output "security_group_id" {
#   description = "The ID of the security group that is created"
#   value       = aws_security_group.api_gateway.id
# }
