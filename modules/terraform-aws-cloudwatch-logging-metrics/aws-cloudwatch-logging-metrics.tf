resource "aws_cloudwatch_log_metric_filter" "logging_metric_filter" {
  name           = var.logging_metric_filter_name
  pattern        = var.filter_pattern
  log_group_name = var.logging_group_name
  metric_transformation {
    name      = var.custom_metric_name
    namespace = var.custom_metric_namespace
    value     = var.metric_value
    unit      = var.metric_unit
  }
}
