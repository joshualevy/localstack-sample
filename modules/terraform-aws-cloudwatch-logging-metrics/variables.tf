terraform {
  # Optional attributes and the defaults function are
  # both experimental, so we must opt in to the experiment.
  experiments = [module_variable_optional_attrs]
}

variable "logging_metric_filter_name" {
  description = "Name of logging metric filter"
  type        = string
}

variable "filter_pattern" {
  description = "Filtering pattern to apply on cloudwatch logs"
  type        = string
}

variable "logging_group_name" {
  description = "Name of cloudwatch logging group where filter needs to be applied"
  type        = string
}

variable "custom_metric_name" {
  description = "Name of custom metric to be published to cloudwatch"
  type        = string
}

variable "custom_metric_namespace" {
  description = "Name of custom metric namespace"
  type        = string
}

variable "metric_value" {
  description = "Value to publish for this metric"
  type        = string
}

variable "metric_unit" {
  description = "Unit for the published metric"
  type        = string
}
