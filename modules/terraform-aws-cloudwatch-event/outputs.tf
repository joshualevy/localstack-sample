output "event_bridge_rule_arn" {
  description = "The arn event shcedulred to moniter health"
  value       = aws_cloudwatch_event_rule.event_rule.arn
}

output "event_name" {
  description = "Name of the event to moniter health"
  value       = aws_cloudwatch_event_rule.event_rule.name
}

output "event_value" {
  description = "Value of the name"
  value       = aws_cloudwatch_event_rule.event_rule.name
}
