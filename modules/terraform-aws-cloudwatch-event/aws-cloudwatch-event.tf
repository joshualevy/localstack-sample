resource "aws_cloudwatch_event_rule" "event_rule" {
  name                = var.event_rule_name
  description         = var.event_rule_description
  schedule_expression = var.event_schedule_expression
}

# resource "aws_cloudwatch_event_target" "invoke_health_check" {
#   arn  = "${var.execution_arn}${var.env_name}/GET/ddbhealth"
#   rule = aws_cloudwatch_event_rule.event_rule.name

#   http_target {
#     query_string_parameters = {
#       type = "healthcheck"
#     }
#   }
# }
