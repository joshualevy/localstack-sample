variable "execution_arn" {
  type        = string
  description = "execution arn to invoke lambda"
}

variable "event_rule_name" {
  type        = string
  description = "Name of the event rule"
}

variable "event_rule_description" {
  type        = string
  description = "Description of the event"
}

variable "event_schedule_expression" {
  type        = string
  description = "Scheduled time to trigger event"
}

variable "env_name" {
  type        = string
  description = "Environment name"
}


