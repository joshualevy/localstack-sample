terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.13.0"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "~> 2.0.0"
    }
  }
}

provider "aws" {
  region = var.env_region
  # access_key = var.aws_account.access_key
  # secret_key = var.aws_account.secret_key
  dynamic "default_tags" {
    for_each = var.custom_tags[*]
    content {
      tags = tomap(default_tags.value)
    }
  }
  access_key = "test"
  secret_key = "test"
  # region                      = "us-east-1"
  s3_use_path_style           = true
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    apigateway     = "http://127.0.0.1:4566"
    cloudformation = "http://127.0.0.1:4566"
    cloudwatch     = "http://127.0.0.1:4566"
    cloudwatchlogs = "http://127.0.0.1:4566"
    dynamodb       = "http://127.0.0.1:4566"
    es             = "http://127.0.0.1:4566"
    firehose       = "http://127.0.0.1:4566"
    iam            = "http://127.0.0.1:4566"
    kinesis        = "http://127.0.0.1:4566"
    lambda         = "http://127.0.0.1:4566"
    route53        = "http://127.0.0.1:4566"
    redshift       = "http://127.0.0.1:4566"
    s3             = "http://s3.localhost.localstack.cloud:4566"
    secretsmanager = "http://127.0.0.1:4566"
    ses            = "http://127.0.0.1:4566"
    sns            = "http://127.0.0.1:4566"
    sqs            = "http://127.0.0.1:4566"
    ssm            = "http://127.0.0.1:4566"
    stepfunctions  = "http://127.0.0.1:4566"
    sts            = "http://127.0.0.1:4566"
  }
}

provider "aws" {
  alias  = "west"
  region = "us-west-2"
  #access_key = var.aws_account.access_key
  #secret_key = var.aws_account.secret_key
  dynamic "default_tags" {
    for_each = var.custom_tags[*]
    content {
      tags = tomap(default_tags.value)
    }
  }
  access_key = "test"
  secret_key = "test"
  # region                      = "us-east-1"
  s3_use_path_style           = true
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    apigateway     = "http://127.0.0.1:4566"
    cloudformation = "http://127.0.0.1:4566"
    cloudwatch     = "http://127.0.0.1:4566"
    cloudwatchlogs = "http://127.0.0.1:4566"
    dynamodb       = "http://127.0.0.1:4566"
    es             = "http://127.0.0.1:4566"
    firehose       = "http://127.0.0.1:4566"
    iam            = "http://127.0.0.1:4566"
    kinesis        = "http://127.0.0.1:4566"
    lambda         = "http://127.0.0.1:4566"
    route53        = "http://127.0.0.1:4566"
    redshift       = "http://127.0.0.1:4566"
    s3             = "http://s3.localhost.localstack.cloud:4566"
    secretsmanager = "http://127.0.0.1:4566"
    ses            = "http://127.0.0.1:4566"
    sns            = "http://127.0.0.1:4566"
    sqs            = "http://127.0.0.1:4566"
    ssm            = "http://127.0.0.1:4566"
    stepfunctions  = "http://127.0.0.1:4566"
    sts            = "http://127.0.0.1:4566"
  }
}

provider "aws" {
  alias  = "edge"
  region = try(var.edge_region, "us-east-1")
  dynamic "default_tags" {
    for_each = var.custom_tags[*]
    content {
      tags = tomap(default_tags.value)
    }
  }
}
