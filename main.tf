resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

module "lambda_functions" {
  source = "./modules/terraform-aws-lambda"

  for_each = merge(local.lambda_functions)

  # custom_tags           = var.custom_tags
  environment_variables = each.value.environment_variables
  function_info = {
    function_description = each.value.description
    function_handler     = "index.handler"
    function_name        = "${var.product_abbreviation}-${each.value.name}"
    function_runtime     = "nodejs14.x"
    # function_runtime     = length(regexall("gis-reverse-proxy", each.value.name)) == 0 ? "nodejs14.x" : "python3.9"
  }

  iam_policy_name = "iam-policy-${var.product_abbreviation}-${each.value.name}-lambda"
  iam_policies    = each.value.iam_policies
  # iam_logs_resources = "arn:${var.partition}:logs:*:*:*"
  output_path = "${path.module}/lambdas/${each.value.name}/${each.value.name}_function_code.zip"
  source_dir  = "${path.module}/lambdas/${each.value.name}/function_code/./"
  timeout     = try(each.value.timeout, 180) #in seconds, defaults to 3 sec max 15 mins
  # vpc_subnet_ids         = data.aws_subnet_ids.subnets.ids
  # vpc_security_group_ids = [module.aws_security_group.security_group_id]
  layers_arn = each.value.layers_arn
  # memory     = try(each.value.memory, var.default_lambda_memory)
}

module "api_gateway" {
  source = "./modules/terraform-aws-api-gateway"

  api_name           = "${var.product_abbreviation}-app-${local.env_config.env_name}"
  binary_media_types = ["*/*"]
  body = templatefile("${path.module}/templates/rest-api-gateway-definition.json", {
    env_region           = var.env_region
    partition            = var.partition
    product_abbreviation = var.product_abbreviation
    env_name             = var.env_name
    stage_name           = local.env_config.rest_api_stage_name
    # cognito_user_pool_arn                                    = module.cognito.cognito_user_pool_arn
    pets_lambda_function_invoke_arn = module.lambda_functions["pets"].lambda_info.lambda_invoke_arn
  })

  custom_tags                             = var.custom_tags
  logging_level                           = "ERROR"
  metrics_enabled                         = "true"
  partition                               = var.partition
  stage_name                              = local.env_config.rest_api_stage_name
  require_authorization_for_cache_control = true
  certificate_arn                         = "aws:acm:us-east-1:000000000000:certificate/52130936-aced-4aa1-ab4f-fab50ca1f5cf"
  domain_name                             = local.env_config.api_domain_name

  depends_on = [
    module.lambda_functions
  ]

  providers = {
    aws      = aws
    aws.west = aws.west
  }
}

output "api_gateway_session" {
  value = {
    hash_key    = module.api_gateway.id
    environment = module.api_gateway.stage_name
    endpoint    = "http://localhost:4566/restapis/${module.api_gateway.id}/localdev/_user_request_/"
  }
}

resource "aws_lambda_permission" "api_gateway_lambda" {
  for_each      = local.lambda_functions
  statement_id  = "${var.product_abbreviation}-AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${var.product_abbreviation}-${each.value.name}"
  principal     = "apigateway.amazonaws.com"
  # source_arn    = "arn:aws:execute-api:${var.env_region}:${data.aws_caller_identity.current.account_id}:${module.api_gateway.id}/*/${each.value.http_method}/${local.lambda_functions[each.key].path_part}"
  # source_arn = "arn:aws:execute-api:${var.env_region}:${data.aws_caller_identity.current.account_id}:${module.api_gateway.id}/*"
  source_arn = "arn:aws:execute-api:${var.env_region}:${data.aws_caller_identity.current.account_id}:${module.api_gateway.id}/*"
}

# resource "aws_iam_user_policy_attachment" "DynamoDBFullAccess" {
#   user       = "terraform"
#   policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
# }

locals {
  dynamodb_table_hack = { for old_key, old_value in local.dynamodb_table :
    (local.env_config.env_name == "dev" && old_key == local.current_misspelling_on_qa_prod ? local.current_spelling_in_dev_tfstate : old_key)
    => old_value
  }
}

module "aws_dynamodb_table" {
  source = "./modules/terraform-aws-dynamodb"

  for_each = local.dynamodb_table_hack

  table_name             = each.value.name
  hash_key               = each.value.hash_key
  range_key              = each.value.range_key
  attributes             = each.value.attributes
  typed_attributes       = try(each.value.typed_attributes, {})
  local_secondary_index  = each.value.local_secondary_index
  global_secondary_index = each.value.global_secondary_index
  env_name               = var.env_name

  # depends_on = [
  #   aws_iam_user_policy_attachment.DynamoDBFullAccess
  # ]
  providers = {
    aws      = aws
    aws.west = aws.west
  }
}

module "envs" {
  source   = "./envs"
  env_name = var.env_name
}

locals {
  env_config = module.envs.env_config
}
