# MVP

- [ ] Has a `pets` Lambda that will:
    [ ] Accept a JSON object as the event with a pet's information
    [ ] Upload the information to an S3 bucket
    [ ] Create a record in a DynamoDB table with the pet's information

- [ ] Has an S3 bucket

- [ ] Has a DynamoDB table

- [ ] Has an API Gateway endpoint that will:
    [ ] Include a schema for the pet's information
    [ ] Support a Dynamo PUT request with a JSON object in the request body

- [ ] Implement Typescript
    [ ] Grab tsconfig.json from sdk repo
