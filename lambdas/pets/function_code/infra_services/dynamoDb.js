const AWS = require('aws-sdk');

class DynamoDbService {
    constructor(dynamoDB, petsTable) {
        this._dynamoDb = dynamoDB;
        this._petsTable = petsTable;
    }

    async submitRequest(newPet) {
        const putParams = {
            TableName: this._petsTable,
            Item: AWS.DynamoDB.Converter.marshall(newPet)
        };

        try {
            await this._dynamoDb.putItem(putParams).promise();
        }
        catch (err) {
            throw err;
        }
    }
}

module.exports = { DynamoDbService };