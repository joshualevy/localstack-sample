const AWS = require('aws-sdk');
const { DynamoDbService } = require('./infra_services/dynamoDb');
const { v4: uuidv4 } = require('uuid');
AWS.config.update({
    region: 'us-east-1'
});
const petsTable = 'Pets';
const dynamoDbService = new DynamoDbService(
    new AWS.DynamoDB({
        endpoint: 'http://host.docker.internal:4566'
    }),
    petsTable
);

exports.handler = async (event) => {
    try {
        const requestId = uuidv4();
        const newPet = {
            id: requestId.toString(),
            ...JSON.parse(event.body)
        };

        console.log("newPet:\n", JSON.stringify(newPet, null, 2));

        await dynamoDbService.submitRequest(newPet);

        return {
            statusCode: 200,
            body: `Pet added to DynamoDB: ${JSON.stringify(newPet, null, 2)}`
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            body: `Error adding pet to DynamoDB. Error: ${err.message}`
        };
    }
};