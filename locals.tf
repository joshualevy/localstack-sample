locals {
  scope_identifier = "pets"
  custom_scopes = [
    { scope_description = "Read access to API", scope_name = "read" }
  ]

  lambda_functions = {
    pets = {
      name        = "pets",
      description = "Function to post pet info to S3 and DynamoDB",
      environment_variables = {
        ENV_NAME   = var.env_name
        PETS_TABLE = local.dynamodb_table.pets_dynamodb_table.name
      },
      iam_policies = [
        "dynamodb:DescribeTable",
        "dynamodb:Scan",
        "dynamodb:Query",
        "dynamodb:UpdateItem",
        "dynamodb:PutItem",
        "dynamodb:GetItem",
        "s3:PutObject",
        "s3:GetObject"
      ],
      layers_arn = []
    }
  }

  current_misspelling_on_qa_prod  = "operartions_dynamodb_table"
  current_spelling_in_dev_tfstate = "operations_dynamodb_table"
  operations_table_name           = local.env_config.env_name == "dev" ? local.current_spelling_in_dev_tfstate : local.current_misspelling_on_qa_prod
  dynamodb_table = {
    pets_dynamodb_table = {
      name                  = "Pets",
      hash_key              = "id",
      range_key             = "firstName",
      local_secondary_index = []
      global_secondary_index = [
        {
          name : "namebreed-index",
          hash_key : "lastName",
          range_key : "breed"
        }
      ]
      attributes = ["id", "firstName", "lastName", "breed"]
    }
  }

  iam_users = {
    lambda_execution_group = {
      user_name = "localdev"
      user_path = "/"
      policy_statements = [
        {
          Action = [
            "lambda:InvokeFunction",
            "lambda:GetFunction"
          ]
          Effect   = "Allow"
          Resource = module.lambda_functions["pets"].lambda_info.lambda_arn
        },
      ]
    }
  }
}
