const apiTestHandler = (payload, context, callback) => {
    console.log("Hello from Lambda!");
    callback(null, {
        statusCode: 200,
        body: JSON.stringify({
            message: 'Hello from Lambda!',
        })
    });
}

module.exports = {
    apiTestHandler,
}