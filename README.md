# LocalStack AWS Docker Environment

## Setup & Installation

### Docker Desktop

**Download Links:**

- [Mac with Apple Silicon Chip](https://desktop.docker.com/mac/main/arm64/Docker.dmg?utm_source=docker&utm_medium=webreferral&utm_campaign=docs-driven-download-mac-arm64)

- [Windows](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe)

### AWS CLI

```bash
brew install awscli
```

### LocalStack

There are a few ways to install LocalStack. But **don't be silly. Just use Homebrew**:

```bash
brew install localstack
```

The LocalStack Cockpit GUI is required:

**NOTE:** It does ask you to give an email for them to send you the download link.

[LocalStack Cockpit](https://localstack.cloud/products/cockpit/)

## Running LocalStack

[LocalStack Dashboard](https://app.localstack.cloud/dashboard)

### Auth Tokens

**NOTE:** In order to use, you'll need to set up a local request certificate, so it won't try use the Xcel AWS auth:

```bash
awslocal acm request-certificate \
    --domain-name <ANY_DOMAIN_HERE> \
    --validation-method DNS \
    --idempotency-token 1234 \
    --options CertificateTransparencyLoggingPreference=DISABLED
```

Then, set the value as the `certificate_arn` value in the `"api_gateway"` module in `main.tf`.

To retrieve your certificate at any point, simply run the following:

```bash
awslocal acm list-certificates
```

**NOTE:** The following is only necessary for certain AWS services. It's not necessary for using a local environment with Lambdas, DynamoDB, and API Gateway. But it does set auth variables that are tied to AWS itself.

```bash
terraform import aws_iam_role.iam_for_lambda iam_for_lambda
```

This will let Terraform manage the auth tokens, which you need to set by running:

```bash
aws configure
```

You may need to edit `role-name` and `IAM-user-name` and run the following:

```bash
aws sts assume-role --role-arn arn:aws:iam::123456789012:role/role-name --role-session-name "RoleSession1" --profile IAM-user-name > assume-role-output.txt
```

### Starting LocalStack

Simply run the following which will spin up the container in Docker

```bash
localstack start
```

### Running Your Code

In a separate terminal, run the following commands in their respective order:

```bash
terraform init
```

```bash
terraform fmt -recursive
```

```bash
terraform validate
```

```bash
terraform plan -var 'env_name=localdev' -var 'partition=aws' -var 'product_abbreviation=gas'
```

```bash
terraform apply -var 'env_name=localdev' -var 'partition=aws' -var 'product_abbreviation=gas' --auto-approve
```

**NOTE:** Use your best judgment on when to use the `--auto-approve` flag in the above command. It allows you to run the complete apply command without having to type `yes` to confirm. So it's convenient when testing multiple changes locally. But run without the flag at least once before submitting a MR.

### Testing

#### API Gateway

**NOTE:** Your API Gateway URL is dynamic. It includes a hashed key that changes with every new `terraform apply`. The structure of the local API URL is:

```bash
http://localhost:4566/restapis/<hash_key>/localdev/_user_request_/<path>
```

When you run a `terraform apply`, the last line printed in console will look like this:

```bash
Apply complete! Resources: 20 added, 0 changed, 0 destroyed.

Outputs:

api_gateway_session = {
  "endpoint" = "http://localhost:4566/restapis/sz34pcs8zg/localdev/_user_request_/"
  "environment" = "localdev"
  "hash_key" = "sz34pcs8zg"
}
```

This is important because with every change, when you run a new `terraform apply`, it generates a new session, with a new hash key for the session. The hash key is in the URL, so a previously tested URL will not work.

You must copy the endpoint URL provided to test API Gateway endpoints.

#### LocalStack Dashboard

If the terraform plan was successfully applied, you should be able to check out the Lambda, API Gateway, and DynamoDB on your [LocalStack Dashboard](https://app.localstack.cloud/)

If you're using the example code, you can visit the `/pets` endpoint in either Postman or Insomnia, and submit the following JSON as your payload:

```json
{
    "firstName": "<your_pets_first_name>",
    "lastName": "<your_pets_last_name>",
    "breed": "<your_pets_breed>"
}
```
