locals {
  env_prd = {
    features = {
      schedule_etl = false
    }

    rest_api_stage_name           = "prd"
    app_auth_identifier           = "gfeeAppprd"
    lambda_function_name          = "API-Lambda"
    dynamodb_iam_role_policy_name = "lambda-dynamodb-log-policy"
    iam_role_Lambda_function      = "IAM-Lambda"
    logging_iam_role_policy_name  = "iam_policy_lambda_logging_function"
    aws_api_gateway_rest_api_name = "Lambda"
    aws_secrets_manager_name      = "kafka-secret-prd"
    vpc_id                        = "vpc-05edd985e85f13724"
    provider_name                 = "PingFedProd"
    api_domain_name               = "gasfee-api-prd.aws.xcelenergy.com"
    subnets = [
      "subnet-034c3d44d89439389",
      "subnet-030bb100fefc44426",
      "subnet-0417df801aa3d46ba"
    ]

    SSORedirectBindingUrl = "https://tsso.xcelenergy.com:443/idp/SSO.saml2"

  }
}
