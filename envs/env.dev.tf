locals {
  env_dev = {
    features = {
      schedule_etl = false
    }

    rest_api_stage_name           = "dev"
    app_auth_identifier           = "gfeeAppdev"
    lambda_function_name          = "API-Lambda"
    dynamodb_iam_role_policy_name = "lambda-dynamodb-log-policy"
    iam_role_Lambda_function      = "IAM-Lambda"
    logging_iam_role_policy_name  = "iam_policy_lambda_logging_function"
    aws_api_gateway_rest_api_name = "Lambda"
    aws_secrets_manager_name      = "kafka-secret-dev"
    vpc_id                        = "vpc-02c30240d3be4f795"
    api_domain_name               = "gasfee-api-dev.aws.xcelenergy.com"
    subnets = [
      "subnet-09f0d0e29c8858a8e",
      "subnet-03cf42b8ace98c2b7",
      "subnet-0c7c0fe57fde1110f"
    ]

    SSORedirectBindingUrl = "https://login.microsoftonline.com/601ff8c0-9157-428b-b436-38feda19daa3/saml2"
  }
}

