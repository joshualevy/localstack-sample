variable "env_name" {
  type = string
}

variable "deployment_slots" {
  type    = map(any)
  default = null
}
