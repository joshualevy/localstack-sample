locals {
  env_localdev = {
    features = {
      schedule_etl = false
    }

    rest_api_stage_name           = "localdev"
    app_auth_identifier           = "gfeeApplocaldev"
    lambda_function_name          = "API-Lambda"
    dynamodb_iam_role_policy_name = "lambda-dynamodb-log-policy"
    iam_role_Lambda_function      = "IAM-Lambda"
    logging_iam_role_policy_name  = "iam_policy_lambda_logging_function"
    aws_api_gateway_rest_api_name = "Lambda"
    aws_secrets_manager_name      = "kafka-sercret-localdev"
    vpc_id                        = "vpc-02c30240d3be4f795"
    api_domain_name               = "127.0.0.1:4566"
    subnets = [
      "subnet-09f0d0e29c8858a8e",
      "subnet-03cf42b8ace98c2b7",
      "subnet-0c7c0fe57fde1110f"
    ]

    SSORedirectBindingUrl = "https://tsso.xcelenergy.com:443/idp/SSO.saml2"
  }
}

