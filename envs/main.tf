locals {
  all_env_configs = {
    default : local.env_default
    localdev : local.env_localdev
    dev : local.env_dev
    qa : local.env_qa
    prd : local.env_prd
  }

  env_config = merge(local.all_env_configs.default, local.all_env_configs[var.env_name])
}
