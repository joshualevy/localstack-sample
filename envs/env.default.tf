locals {
  env_default = {
    env_name = var.env_name

    cognito_user_pool_name = "cognito-user-pool"
    user_pool_client_name  = "cognito-user-pool-client"

    provider_name = "AzureADNonProd"

    aws_region = "us-east-2"

    xml_file_name = "metadata_Xcel_TSSO_NonProd"
  }
}
